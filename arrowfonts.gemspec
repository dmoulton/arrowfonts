# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'arrowfonts/version'

Gem::Specification.new do |gem|
  gem.name          = "arrowfonts"
  gem.version       = Arrowfonts::VERSION
  gem.authors       = ["David Moulton"]
  gem.email         = ["dave@themoultons.net"]
  gem.description   = %q{Arrow fonts}
  gem.summary       = %q{Four arrows to use with variance charts}
  gem.homepage      = ""

  gem.files = Dir["{lib,vendor}/**/*"] + ["LICENSE.txt", "README.md"]
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
  gem.add_dependency "railties", "~> 3.1"
end
